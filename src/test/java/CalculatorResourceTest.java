import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+100";
        assertEquals(500, Integer.parseInt(calculatorResource.calculate(expression)));

        expression = " 300 - 99-1 ";
        assertEquals(200, Integer.parseInt(calculatorResource.calculate(expression)));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+4";
        assertEquals(404, calculatorResource.sum(expression));

        expression = "300+99+7";
        assertEquals(406, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100-99";
        assertEquals(800, calculatorResource.subtraction(expression));

        expression = "20-2-8";
        assertEquals(10, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10*10*10";
        assertEquals(1000, calculatorResource.multiplication(expression));

        expression = "20*2*6";
        assertEquals(240, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10/10/1";
        assertEquals(1, calculatorResource.division(expression));

        expression = "20/2/2";
        assertEquals(5, calculatorResource.division(expression));

    }
}
