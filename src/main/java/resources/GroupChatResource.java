package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO gcd = new GroupChatDAO();
        GroupChat tempGroup =  gcd.getGroupChat(groupChatId);
        tempGroup.setMessageList(gcd.getGroupChatMessages(groupChatId));
        tempGroup.setUserList(gcd.getGroupChatUsers(groupChatId));
        return tempGroup;
    }

    /**
     * GET method to get the group chats from user
     * @param userId of the user to GET
     * @return GroupChat list
     */
    @GET
    @Path ("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUserId(@PathParam("userId") int userId){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.getGroupChatByUserId(userId);
    }

    /**
     * GET method to get messages in group chat
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessages(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.getGroupChatMessages(groupChatId);
    }

    /**
     * GET method to get messages in group chat
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @POST
    @Path ("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    @Consumes (MediaType.APPLICATION_JSON)
    public Message addMessage(@PathParam("groupChatId") int groupChatId, Message message){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.addMessage(groupChatId, message);
    }

    /**
     * add a group chat
     * @param groupChat group chat to add
     * @return added grupeChat
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat addGroupChat(GroupChat groupChat){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.addGroupChat(groupChat);
    }
}
